package account

import (
	"errors"
	"net/http"
	"time"

	libhttp "bitbucket.org/jacobkranz/clickigence-lib-http"
	structs "bitbucket.org/jacobkranz/clickigence-lib-structs"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
)

// Interface defines our interface for the collect package
type Interface interface {
	Set(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
}

type account struct {
	db dbInterface
}

// NewAccount creates a struct that implements the interface
func NewAccount(dbConn *gocql.Session) Interface {
	return &account{db: newDb(dbConn)}
}

func (a *account) Set(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	setReq := &structs.AccountSetRequest{}
	code, err := libhttp.HandleRequest(setReq, r)
	if err != nil {
		libhttp.Write(w, code, err)
		return
	}

	dbAccount, err := a.db.GetByURL(setReq.URL)
	if err != nil {
		libhttp.Write(w, code, err)
		return
	}

	if dbAccount != nil {
		libhttp.Write(w, http.StatusBadRequest, errors.New("account already exists with that url"))
		return
	}

	id, err := gocql.RandomUUID()
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbInsert := &structs.Account{
		URL:     setReq.URL,
		Name:    setReq.Name,
		Created: time.Now(),
		ID:      id,
	}

	if err := a.db.Insert(dbInsert); err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	accessToken, err := libhttp.GetAccessTokenString(r)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	if err := a.db.Insert(dbInsert); err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	api := libhttp.NewAPI(&libhttp.Config{
		Host:        "http://localhost:5000",
		AccessToken: accessToken,
	})

	_, err = api.UserSetAccount(&structs.UsersSetAccountRequest{AccountID: dbInsert.ID.String()})
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	libhttp.Write(w, http.StatusOK, dbInsert)
}

func (a *account) Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	accessToken, err := libhttp.GetAccessTokenString(r)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	api := libhttp.NewAPI(&libhttp.Config{
		Host:        "http://localhost:5000",
		AccessToken: accessToken,
	})

	user, err := api.UserGet()
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbAccount, err := a.db.GetByID(user.AccountID.String())
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	if dbAccount == nil {
		libhttp.Write(w, http.StatusNotFound, errors.New("account not found for user"))
		return
	}

	libhttp.Write(w, http.StatusOK, dbAccount)
}
