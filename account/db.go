package account

import (
	structs "bitbucket.org/jacobkranz/clickigence-lib-structs"
	"github.com/gocql/gocql"
	"github.com/mitchellh/mapstructure"
)

type dbInterface interface {
	Insert(*structs.Account) error
	GetByID(ID string) (*structs.Account, error)
	GetByURL(URL string) (*structs.Account, error)
}

type db struct {
	cassandra *gocql.Session
}

func newDb(conn *gocql.Session) dbInterface {
	return &db{cassandra: conn}
}

func (db *db) Insert(account *structs.Account) error {
	err := db.cassandra.Query(`INSERT INTO clickigence.accounts_by_id (name, id, url, created) VALUES (?, ?, ?, ?)`,
		account.Name, account.ID, account.URL, account.Created).Exec()
	if err != nil {
		return err
	}

	return db.cassandra.Query(`INSERT INTO clickigence.accounts_by_url (name, id, url, created) VALUES (?, ?, ?, ?)`,
		account.Name, account.ID, account.URL, account.Created).Exec()
}

func (db *db) GetByID(ID string) (*structs.Account, error) {
	m := map[string]interface{}{}

	iter := db.cassandra.Query(`select * from clickigence.accounts_by_id where id=?`, ID).Iter()
	defer iter.Close() // make sure we close this!
	for iter.MapScan(m) {
		account := &structs.Account{}
		return account, mapstructure.Decode(m, account) // we only want the first account and only the first user
	}
	return nil, nil // no account so make nil & error nil
}

func (db *db) GetByURL(URL string) (*structs.Account, error) {
	m := map[string]interface{}{}

	iter := db.cassandra.Query(`select * from clickigence.accounts_by_url where url=?`, URL).Iter()
	defer iter.Close() // make sure we close this!
	for iter.MapScan(m) {
		account := &structs.Account{}
		return account, mapstructure.Decode(m, account) // we only want the first account and only the first user
	}
	return nil, nil // no account so make nil & error nil
}
