package main

import (
	"github.com/gocql/gocql"
)

func (s *Service) setDB() (*gocql.Session, error) {
	cluster := gocql.NewCluster("localhost")
	cluster.Authenticator = gocql.PasswordAuthenticator{
		Username: s.config.CassandraUser,
		Password: s.config.CassandraPassword,
	}
	//cluster.ProtoVersion = 4
	cluster.Keyspace = s.config.CassandraKeySpace
	return cluster.CreateSession()
}
