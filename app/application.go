package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/jacobkranz/clickigence-api/account"
	"bitbucket.org/jacobkranz/clickigence-api/collect"
	"bitbucket.org/jacobkranz/clickigence-api/site"
	"bitbucket.org/jacobkranz/clickigence-api/user"
	"bitbucket.org/jacobkranz/library_config"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
)

type Config struct {
	CassandraHost     string `env:"CASSANDRA_HOST"`
	CassandraPort     string `env:"CASSANDRA_PORT"`
	CassandraUser     string `env:"CASSANDRA_USER"`
	CassandraPassword string `env:"CASSANDRA_PASSWORD"`
	CassandraKeySpace string `env:"CASSANDRA_KEYSPACE"`
	BaseURL           string `env:"CLICKIGENCE_BASE_URL"`
}

type Service struct {
	db      *gocql.Session
	config  *Config
	User    user.Interface
	Collect collect.Interface
	Account account.Interface
	Site    site.Interface
}

func main() {

	s := &Service{
		config: &Config{},
	}

	err := libcfg.ParseEnv(s.config, os.LookupEnv)
	if err != nil {
		panic(err)
	}

	s.db, err = s.setDB()
	if err != nil {
		panic(err)
	}

	s.User = user.NewUser(s.db)
	s.Collect = collect.NewCollect(s.db)
	s.Account = account.NewAccount(s.db)
	s.Site = site.NewSite(s.db)

	router := httprouter.New()
	router.GET("/", s.Index)

	router.POST("/accounts", s.Account.Set)
	router.GET("/accounts", s.Account.Get)

	router.POST("/sites", s.Site.Set)
	router.GET("/sites/:id", s.Site.Get)
	router.GET("/sites", s.Site.List)

	router.POST("/users/signup", s.User.Signup)
	router.POST("/users/signin", s.User.Signin)
	router.POST("/users/signout", s.User.Signout)
	router.GET("/users", s.AuthMiddleware(s.User.Get))
	router.POST("/users/set_account", s.AuthMiddleware(s.User.SetAccount))

	router.POST("/collect/page", s.Collect.Page)
	router.POST("/collect/time", s.Collect.Time)

	log.Fatal(http.ListenAndServe(":5000", router))
}

// Index just returns 200 to report the api is up
func (s *Service) Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Status OK"))
}

// AuthMiddleware function, which will be called for each request
func (s *Service) AuthMiddleware(nextCall httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if !s.User.Verify(r) {
			w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		nextCall(w, r, ps)
	}
}
