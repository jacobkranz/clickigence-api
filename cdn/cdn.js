

function cga(b) {

	this.businessId 	= null;
	this.domainName 	= null;
	this.activeSession 	= true;
	this.session 		= null;

	this.setEvent = function(eventType, category, values) {

		// All HTML5 Rocks properties support CORS.
		var url = 'http://localhost:7000/click/set?'
		// alphabetical
		 +'BusinessId='+encodeURIComponent(this.businessId)
		+'&Category='+encodeURIComponent(category)
		+'&Date='+encodeURIComponent(getDateTime())
		+'&Domain='+encodeURIComponent(this.domainName)
		+'&EventType='+encodeURIComponent(eventType)
		+'&Referrer='+encodeURIComponent(document.referrer)
		+'&TimeZone='+encodeURIComponent(new Date().getTimezoneOffset() / 60)
		+'&Url='+encodeURIComponent(document.URL)
		+'&Visitor='+encodeURIComponent(cookie())
		+'&VisitorSession='+encodeURIComponent(session());

		if( values instanceof Array )
		{
			url += "&Values="+JSON.stringify(values);
		}
		else
		{
			url += "&Values="+encodeURIComponent(values);
		}

		sendCors(url, 'GET', true);

	}



	// Helper method to parse the title tag from the response.
	function getTitle(text)
	{
		return text;
	}

		// Make the actual CORS request.


	// COOKIE FUNCTIONS

	function cookie()
	{
		// get cookie else set one and return the value
		var value = "; " + document.cookie;
		var parts = value.split("; _cga=");
		if (parts.length == 2)
			return parts.pop().split(";").shift();
		else
			return setCookie();
	}

	function setCookie()
	{
		// get random string for our unique identifier
		var value = randomString(40);

		//'cookie1=test; expires=Fri, 3 Aug 2001 20:47:11 UTC; path=/'
		document.cookie = '_cga='+value+'; expires=Wed, 1 Jan 2025 10:10:10 UTC; path=/';
		return value;
	}

	// SESSION FUNCTIONS
	function session()
	{
		if( this.session == null )
			this.session = randomString(10);
		return this.session;
	}

	function randomString(num)
	{
		// pretty self-explanatory
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < num; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	}

	function getDateTime() {
		var now     = new Date(); 
		var year    = now.getFullYear();
		var month   = now.getMonth()+1; 
		var day     = now.getDate();

		year = year.toString().substr(2,2);

		if(day.toString().length == 1) {
			var day = '0'+day;
		}
		if(month.toString().length == 1) {
			var month = '0'+month;
		}
		if(year.toString().length == 1) {
			var year = '0'+year;
		}

		var dateTime = day+':'+month+':'+year;   
		return dateTime;
	}


////
//// SESSION POLLING
//// determines time on site
////
// https://developer.mozilla.org/en-US/docs/Web/Guide/User_experience/Using_the_Page_Visibility_API
	var hidden;
	var visibilityChange; 
	//var endpoint = "<?php echo ( \Config::get('constants.LOCALHOST') || \Config::get('constants.STAGE') ) ? 'http://stage.clickigence.com/click/poll/set' : 'https://clickigence.com/click/poll/set'; ?>";
	var endpoint = '';
	var timeOnSite = 0;

	if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
		hidden = "hidden";
		visibilityChange = "visibilitychange";
	} else if (typeof document.mozHidden !== "undefined") {
		hidden = "mozHidden";
		visibilityChange = "mozvisibilitychange";
	} else if (typeof document.msHidden !== "undefined") {
		hidden = "msHidden";
		visibilityChange = "msvisibilitychange";
	} else if (typeof document.webkitHidden !== "undefined") {
		hidden = "webkitHidden";
		visibilityChange = "webkitvisibilitychange";
	}

	function handleVisibilityChange() {
		if (document[hidden]) {
			this.activeSession = false;
		} else {
			this.activeSession = true;
		}
	}

	function addEvent(object, event_type, event_handler) {
		if (object.addEventListener) {
			object.addEventListener(event_type, event_handler, false);
		} else {
			object.attachEvent("on" + event_type, handler);
		}
	}


	if (typeof document.addEventListener === "undefined" || 
		typeof hidden === "undefined") {
			// sessions are not allowed here
	} else {
		// Handle page visibility change   
		document.addEventListener(visibilityChange, handleVisibilityChange, false);
	}

	// now set this.activeSession
	handleVisibilityChange();
	// set the onBeforeUnload event

	// poll every second
	setInterval(poll, 1000);

	function poll( )
	{
		if( this.activeSession )
		{
			timeOnSite += 1;
		}
	}

	function setSessionLength()
	{
		var endpoint = "http://localhost:7000/click/visitor/time/set?ClickSession="+session()+"&ClickVisitor"+cookie()+"&TimeOnSite="+timeOnSite;
		sendCors(endpoint, 'GET', false);
		return '';
	}

	window.onunload = setSessionLength;






////
//// CORS
////
	function sendCors(url, method, async) {

		var xhr = createCORSRequest(method, url, async);

		if (!xhr)
		{
			console.log('no support for cga.cors');
			return;
		}

		// Response handlers.
		xhr.onload = function()
		{
			console.log(xhr.responseText);
		};

		xhr.onerror = function()
		{
			console.log('error in cga.cors');
		};

		xhr.send();
	}

	// CORS FUNCTIONS
	function createCORSRequest(method, url, async)
	{
		var xhr = new XMLHttpRequest();
		if ("withCredentials" in xhr)
		{
			// XHR for Chrome/Firefox/Opera/Safari.
			// last request is for ASYNC to be enabled / disabled
			xhr.open(method, url, async);
		} 
		else if (typeof XDomainRequest != "undefined")
		{
			// XDomainRequest for IE.
			xhr = new XDomainRequest();
			xhr.open(method, url);
		}
		else
		{
			// CORS not supported.
			xhr = null;
		}
		return xhr;
	}

	function setClosure(closureVar)
	{
		if(typeof closureVar !== 'undefined'){
   			closureVar();
 		};
	}
}