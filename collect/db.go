package collect

import "github.com/gocql/gocql"

type dbInterface interface {
}

type db struct {
	cassandra *gocql.Session
}

func newDb(conn *gocql.Session) dbInterface {
	return &db{cassandra: conn}
}
