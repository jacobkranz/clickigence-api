package collect

import (
	"net/http"

	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
)

// Interface defines our interface for the collect package
type Interface interface {
	Page(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Time(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
}

type collect struct {
	db dbInterface
}

// NewCollect creates a struct that implements the interface
func NewCollect(dbConn *gocql.Session) Interface {
	return &collect{db: newDb(dbConn)}
}

func (c *collect) Page(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

}

func (c *collect) Time(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

}
