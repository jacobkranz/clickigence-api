package user

import (
	"github.com/mitchellh/mapstructure"

	structs "bitbucket.org/jacobkranz/clickigence-lib-structs"
	"github.com/gocql/gocql"
)

type dbInterface interface {
	Insert(*structs.User) error
	GetUserByEmail(email string) (*structs.User, error)
	GetUserByID(ID string) (*structs.User, error)
	SetAccount(*structs.User) error
}

type db struct {
	cassandra *gocql.Session
}

func newDb(conn *gocql.Session) dbInterface {
	return &db{cassandra: conn}
}

func (db *db) GetUserByEmail(email string) (*structs.User, error) {
	m := map[string]interface{}{}

	iter := db.cassandra.Query(`select * from clickigence.users_by_email where email=?`, email).Iter()
	defer iter.Close() // make sure we close this!
	for iter.MapScan(m) {
		user := &structs.User{}
		return user, mapstructure.Decode(m, user) // we only want the first user and only the first user
	}
	return nil, nil // no user so make nil & error nil
}

func (db *db) GetUserByID(id string) (*structs.User, error) {
	m := map[string]interface{}{}

	iter := db.cassandra.Query(`select * from clickigence.users_by_id where id=?`, id).Iter()
	defer iter.Close() // make sure we close this!
	for iter.MapScan(m) {
		user := &structs.User{}
		return user, mapstructure.Decode(m, user) // we only want the first user and only the first user
	}
	return nil, nil // no user so make nil & error nil
}

func (db *db) Insert(user *structs.User) error {
	err := db.cassandra.Query(`INSERT INTO clickigence.users_by_email (id, account_id, email, password, first_name, last_name, created) VALUES (?, ?, ?, ?, ?, ?, ?)`,
		user.ID, user.AccountID, user.Email, user.Password, user.FirstName, user.LastName, user.Created).Exec()
	if err != nil {
		return err
	}

	return db.cassandra.Query(`INSERT INTO clickigence.users_by_id (id, account_id, email, password, first_name, last_name, created) VALUES (?, ?, ?, ?, ?, ?, ?)`,
		user.ID, user.AccountID, user.Email, user.Password, user.FirstName, user.LastName, user.Created).Exec()
}

func (db *db) SetAccount(user *structs.User) error {
	err := db.cassandra.Query(`update users_by_id set account_id=? where id=?`,
		user.AccountID, user.ID).Exec()
	if err != nil {
		return err
	}

	return db.cassandra.Query(`update users_by_email set account_id=? where email=?`,
		user.AccountID, user.Email).Exec()
}
