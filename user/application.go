package user

import (
	"errors"
	"net/http"
	"time"

	libhttp "bitbucket.org/jacobkranz/clickigence-lib-http"
	structs "bitbucket.org/jacobkranz/clickigence-lib-structs"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

// Interface defines the User's api
type Interface interface {
	Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Signup(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Signin(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Signout(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	SetAccount(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Verify(r *http.Request) bool
}

type user struct {
	db dbInterface
}

// NewUser creates a struct that implements the interface
func NewUser(dbConn *gocql.Session) Interface {
	return &user{db: newDb(dbConn)}
}

func (u *user) Signup(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	signup := &structs.UserSignupRequest{}
	code, err := libhttp.HandleRequest(signup, r)
	if err != nil {
		libhttp.Write(w, code, err)
		return
	}

	bytes, err := bcrypt.GenerateFromPassword([]byte(signup.Password), 14)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}
	signup.Password = string(bytes)

	dbUser, err := u.db.GetUserByEmail(signup.Email)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}
	if dbUser != nil {
		libhttp.Write(w, http.StatusBadRequest, errors.New("user already exists with that email"))
		return
	}

	id, err := gocql.RandomUUID()
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbUser = &structs.User{
		// ommit account_id; account.Set will call user.Update that'll update the account id
		ID:        id,
		FirstName: signup.FirstName,
		LastName:  signup.LastName,
		Email:     signup.Email,
		Password:  signup.Password,
		Created:   time.Now(),
		IsAdmin:   false,
	}

	err = u.db.Insert(dbUser)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbUser, err = u.db.GetUserByEmail(signup.Email)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	token, err := libhttp.CreateAccessToken(dbUser.ID, true)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	libhttp.Write(w, http.StatusOK, &structs.UserSignupResponse{AccessToken: token})
}

func (u *user) Signin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	signin := &structs.UserSigninRequest{}
	code, err := libhttp.HandleRequest(signin, r)
	if err != nil {
		libhttp.Write(w, code, err)
		return
	}

	dbUser, err := u.db.GetUserByEmail(signin.Email)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}
	if dbUser == nil {
		libhttp.Write(w, http.StatusBadRequest, errors.New("username either does not exist or password is invalid"))
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(dbUser.Password), []byte(signin.Password)); err != nil {
		libhttp.Write(w, http.StatusBadRequest, errors.New("username either does not exist or password is invalid"))
		return
	}

	token, err := libhttp.CreateAccessToken(dbUser.ID, true)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	libhttp.Write(w, http.StatusOK, &structs.UserSigninResponse{AccessToken: token})
}

func (u *user) Signout(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Write([]byte("User Signout is HERE!"))
}

func (u *user) Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	accessToken, err := libhttp.GetAccessToken(r)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbUser, err := u.db.GetUserByID(accessToken.ID.String())
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	if dbUser == nil {
		libhttp.Write(w, http.StatusNotFound, errors.New("user not found"))
		return
	}

	libhttp.Write(w, http.StatusOK, dbUser)
}

func (u *user) SetAccount(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	signin := &structs.UsersSetAccountRequest{}
	code, err := libhttp.HandleRequest(signin, r)
	if err != nil {
		libhttp.Write(w, code, err)
		return
	}

	accessToken, err := libhttp.GetAccessToken(r)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbUser, err := u.db.GetUserByID(accessToken.ID.String())
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	if dbUser == nil {
		libhttp.Write(w, http.StatusInternalServerError, errors.New("error fetching user"))
		return
	}

	uuid, err := uuid.FromString(signin.AccountID)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbUser.AccountID, err = gocql.UUIDFromBytes(uuid.Bytes())
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	err = u.db.SetAccount(dbUser)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, errors.New("error fetching user"))
		return
	}

	libhttp.Write(w, http.StatusOK, dbUser)
}

func (u *user) Verify(r *http.Request) bool {
	accessToken, err := libhttp.GetAccessToken(r)
	return (err == nil && accessToken != nil)
}
