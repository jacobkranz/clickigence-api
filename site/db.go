package site

import (
	structs "bitbucket.org/jacobkranz/clickigence-lib-structs"
	"github.com/gocql/gocql"
	"github.com/mitchellh/mapstructure"
)

type dbInterface interface {
	GetByID(string, string) (*structs.Site, error)
	GetByAccountID(string) ([]*structs.Site, error)
	GetByURL(string) (*structs.Site, error)
	Insert(*structs.Site) error
}

type db struct {
	cassandra *gocql.Session
}

func newDb(conn *gocql.Session) dbInterface {
	return &db{cassandra: conn}
}

func (db *db) GetByID(accountID, ID string) (*structs.Site, error) {
	m := map[string]interface{}{}

	iter := db.cassandra.Query(`select * from clickigence.sites_by_account where account_id=? and id=?`, accountID, ID).Iter()
	defer iter.Close() // make sure we close this!
	for iter.MapScan(m) {
		site := &structs.Site{}
		return site, mapstructure.Decode(m, site) // we only want the first account and only the first user
	}
	return nil, nil // no account so make nil & error nil
}

func (db *db) GetByURL(URL string) (*structs.Site, error) {
	m := map[string]interface{}{}

	iter := db.cassandra.Query(`select * from clickigence.sites_by_url where url=?`, URL).Iter()
	defer iter.Close() // make sure we close this!
	for iter.MapScan(m) {
		site := &structs.Site{}
		return site, mapstructure.Decode(m, site) // we only want the first account and only the first user
	}
	return nil, nil // no account so make nil & error nil
}

func (db *db) GetByAccountID(ID string) ([]*structs.Site, error) {
	m := map[string]interface{}{}
	sites := []*structs.Site{}

	iter := db.cassandra.Query(`select * from clickigence.sites_by_account where account_id=?`, ID).Iter()
	defer iter.Close() // make sure we close this!
	for iter.MapScan(m) {
		site := &structs.Site{}
		err := mapstructure.Decode(m, site) // we only want the first account and only the first user
		if err != nil {
			return nil, err
		}

		sites = append(sites, site)
		m = map[string]interface{}{} // why the FUCK we have to do this? no idea... last comment on: https://github.com/gocql/gocql/issues/677
	}
	return sites, nil // no account so make nil & error nil
}

func (db *db) Insert(site *structs.Site) error {
	err := db.cassandra.Query(`INSERT INTO clickigence.sites_by_account (id, account_id, url, created) VALUES (?, ?, ?, ?)`,
		site.ID, site.AccountID, site.URL, site.Created).Exec()
	if err != nil {
		return err
	}

	return db.cassandra.Query(`INSERT INTO clickigence.sites_by_url (id, account_id, url, created) VALUES (?, ?, ?, ?)`,
		site.ID, site.AccountID, site.URL, site.Created).Exec()
}
