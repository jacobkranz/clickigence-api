package site

import (
	"errors"
	"net/http"
	"time"

	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"

	libhttp "bitbucket.org/jacobkranz/clickigence-lib-http"
	structs "bitbucket.org/jacobkranz/clickigence-lib-structs"
)

// Interface defines our interface for the collect package
type Interface interface {
	Set(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	List(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
}

type site struct {
	db dbInterface
}

// NewSite creates a struct that implements the interface
func NewSite(dbConn *gocql.Session) Interface {
	return &site{db: newDb(dbConn)}
}

func (s *site) Set(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	setReq := &structs.SiteSetRequest{}
	code, err := libhttp.HandleRequest(setReq, r)
	if err != nil {
		libhttp.Write(w, code, err)
		return
	}

	dbSite, err := s.db.GetByURL(setReq.URL)
	if err != nil {
		libhttp.Write(w, code, err)
		return
	}
	if dbSite != nil {
		libhttp.Write(w, http.StatusBadRequest, errors.New("site already exists with that url"))
		return
	}

	accessToken, err := libhttp.GetAccessTokenString(r)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	api := libhttp.NewAPI(&libhttp.Config{
		Host:        "http://localhost:5000",
		AccessToken: accessToken,
	})

	account, err := api.AccountGet()
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	id, err := gocql.RandomUUID()
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbSite = &structs.Site{
		ID:        id,
		AccountID: account.ID,
		URL:       setReq.URL,
		Created:   time.Now(),
	}

	if err := s.db.Insert(dbSite); err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	libhttp.Write(w, http.StatusOK, dbSite)
}

func (s *site) Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	accessToken, err := libhttp.GetAccessTokenString(r)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	api := libhttp.NewAPI(&libhttp.Config{
		Host:        "http://localhost:5000",
		AccessToken: accessToken,
	})

	account, err := api.AccountGet()
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbSite, err := s.db.GetByID(account.ID.String(), ps.ByName("id"))
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	libhttp.Write(w, http.StatusOK, dbSite)
}

func (s *site) List(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	accessToken, err := libhttp.GetAccessTokenString(r)
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	api := libhttp.NewAPI(&libhttp.Config{
		Host:        "http://localhost:5000",
		AccessToken: accessToken,
	})

	account, err := api.AccountGet()
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	dbSites, err := s.db.GetByAccountID(account.ID.String())
	if err != nil {
		libhttp.Write(w, http.StatusInternalServerError, err)
		return
	}

	libhttp.Write(w, http.StatusOK, dbSites)
}
